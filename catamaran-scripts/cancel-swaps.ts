import {
  callReadOnlyFunction,
  ClarityType,
  contractPrincipalCV,
  cvToHex,
  cvToString,
  hexToCV,
  makeContractCall,
  PostConditionMode,
  ResponseOkCV,
  uintCV,
  UIntCV,
} from "@stacks/transactions";
import BN from "bn.js";
import { Pool } from "pg";
import { handleTransaction, mainnet, network } from "../src/deploy";
import { accountsApi, contractsApi } from "../src/pool-tool-utils";
import * as fs from "fs";

export const user = mainnet
  ? JSON.parse(fs.readFileSync("./catamaran-scripts/user.json").toString())
  : {};

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "postgres",
  password: "postgres",
  port: 5555,
});

pool.query(
  `select contract_call_function_name, contract_call_function_args , * from txs t 
  where canonical = true and microblock_canonical = true 
  and contract_call_function_name = 'create-swap'  
  and contract_call_contract_id in ('SP2PABAF9FTAJYNFZH93XENAJ8FVY99RRM50D2JG9.btc-nft-swap-v1', 'SP2PABAF9FTAJYNFZH93XENAJ8FVY99RRM50D2JG9.btc-ft-swap')
  `,
  async (error: any, results: any) => {
    if (error) {
      throw error;
    }
    const swaps = results.rows.map((r: any) => {
      const resultCV = hexToCV(r.raw_result.toString("hex"));
      return resultCV.type === ClarityType.ResponseOk
        ? {
            swapId: (resultCV.value as UIntCV).value.toNumber(),
            contract: r.contract_call_contract_id.split("."),
          }
        : undefined;
    });
    const swapState = [];
    let count = 0;
    const accountInfo = await accountsApi.getAccountInfo({
      principal: user.stacks,
    });
    const nonce = 0; //accountInfo.nonce;
    for (let s of swaps) {
      if (s) {
        const stateHex = await contractsApi.getContractDataMapEntry({
          contractAddress: s.contract[0],
          contractName: s.contract[1],
          mapName: "swaps",
          key: cvToHex(uintCV(s.swapId)),
        });
        const dataCV = hexToCV(stateHex.data) as any;
        console.log(
          s.swapId,
          s.contract[1],
          cvToString(dataCV.value.data.done)
        );
        if (dataCV.value.data.done.value.toNumber() === 0) {
          const token = cvToString(
            s.contract[1] === "btc-ft-swap"
              ? dataCV.value.data.ft
              : dataCV.value.data.nft
          );
          const [contract, asset] = token.split("::");
          const [contractAddress, contractName] = contract.split(".");
          console.log({ contractAddress, contractName });
          const tx = await makeContractCall({
            contractAddress: s.contract[0],
            contractName: s.contract[1],
            functionName: "cancel",
            functionArgs: [
              uintCV(s.swapId),
              contractPrincipalCV(contractAddress, contractName),
            ],
            nonce: new BN(nonce + count),
            fee: new BN(100000),
            network: network,
            senderKey: user.private,
            postConditionMode: PostConditionMode.Allow,
          });
          const txId = await handleTransaction(tx);
          console.log(nonce + count, txId);
          count += 1;
          swapState.push({
            txId,
            swapsId: s.swapId,
            contract: s.contract,
            nonce: nonce + count,
          });
        }
      }
    }
    console.log(swapState);
  }
);
