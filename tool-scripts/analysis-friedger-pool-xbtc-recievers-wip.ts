
  for (let s of filteredExtendedStackers) {
    const xbtcRewardsEntry = await contractsApi.getContractDataMapEntry({
      contractAddress: FRIEDGER_POOL_XBTC.address,
      contractName: FRIEDGER_POOL_XBTC.name,
      mapName: "xbtc-rewards",
      key: cvToHex(standardPrincipalCV(s.stacker)),
      proof: 0,
    });
    const entryCV = hexToCV(xbtcRewardsEntry.data);
    const xbtcRewards =
      entryCV.type === ClarityType.OptionalSome &&
      entryCV.value.type === ClarityType.BoolTrue;
    console.log(xbtcRewards);
  }
