import { writeFileSync } from "fs";
import { network, NEXT_CYCLE } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  downloadBoomboxesTxs,
  boomboxes,
  getStackersFromBoombox,
  downloadPoxTxs,
  getStackersFromPox,
  boomboxes2,
  getStackersFromBoomboxAdmin,
  downloadBoomboxAdminTxs,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const cycle = NEXT_CYCLE; // <--- edit here

const { poolAdmin, pool3cycles, pool6cycles, pool700 } = keys;
(async () => {
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoxTxs(network);
  await downloadBoomboxAdminTxs(network);

  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    "SPSTX06BNGJ2CP1F6WA8V49B6MYD784N6YZMK95G" // exclude stx foundation pool
  );
  const poxStackers = getStackersFromPox(network, [
    poolAdmin,
    pool3cycles,
    pool6cycles,
    pool700,
  ]);
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ boomboxCount: boomboxStackers.length });
  /*
  const boomboxStackers2 = getStackersFromBoombox(
    `${boomboxes2.address}.${boomboxes2.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ boomboxCount2: boomboxStackers2.length });
*/

  const boomboxAdminStackers = await getStackersFromBoomboxAdmin(
    16,
    48,
    1,
    network
  );
  console.log({ boomboxAdminCount: boomboxAdminStackers.length });

  const stackers = v0Stackers
    .concat(v1Stackers)
    .concat(poxStackers)
    .concat(boomboxStackers)
    .concat(boomboxAdminStackers);
  //.concat(boomboxStackers2);


  // only logging
  const boomboxStackersInCycle = stackersToCycle(boomboxStackers, cycle);
  console.log({ boomboxCountInCycle: boomboxStackersInCycle.members.length });

  const boomboxAdminStackersInCycle = stackersToCycle(
    boomboxAdminStackers,
    cycle
  );
  console.log({
    boomboxAdminCountInCycle: boomboxAdminStackersInCycle.members.length,
  });

  const stackersInCycle = stackersToCycle(stackers, cycle);
  writeFileSync(`members-${cycle}.json`, JSON.stringify(stackersInCycle));
})();
