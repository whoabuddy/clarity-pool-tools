import { ReadOnlyFunctionArgsFromJSON } from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  bufferCVFromString,
  createAssetInfo,
  cvToHex,
  cvToString,
  falseCV,
  FungibleConditionCode,
  hexToCV,
  listCV,
  makeContractCall,
  makeStandardFungiblePostCondition,
  noneCV,
  parseReadOnlyResponse,
  ReadOnlyFunctionResponse,
  someCV,
  sponsorTransaction,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import { accountsApi, bnsApi, contractsApi } from "../src/pool-tool-utils";
import { keys } from "./config";
import { SEND_XBTC_MANY_CONTRACT, XBTC_CONTRACT } from "../src/constants";
import { readFileSync } from "fs";

const BN = require("bn.js");

const { poolAdmin, poolStxFoundation } = keys;

const dryrun = false;
const sponsored = true;
const payoutFee = new BN(7_000);
const filename = "distribution-2022-11-13T053002.112Z" + ".csv";
const memo = filename.substring(13, 23) + " " + "xQNCwjt1Bv1aK0Y5Lt9q5A";
console.log({ memo, filename });
const grouped = true;
const firstChaining = true; // <---- edit here, set to false to use nonces below
const noncesForRepeatedChaining =
  // nonces                 // <---- edit here
  { nonce: 580, sponsorNonce: 4744 };

async function payoutAdvocat(
  details: { amountXBTC: number; recipient: string }[],
  nonce: number,
  sponsorNonce: number
) {
  let total = details.reduce((sum, d) => sum + d.amountXBTC, 0);
  let options;
  if (details.length === 1) {
    const { amountXBTC, recipient } = details[0];
    options = {
      contractAddress: XBTC_CONTRACT.address,
      contractName: XBTC_CONTRACT.name,
      functionName: "transfer",
      functionArgs: [
        uintCV(amountXBTC),
        standardPrincipalCV(poolStxFoundation.stacks),
        standardPrincipalCV(recipient),
        someCV(bufferCVFromString(memo)),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          poolStxFoundation.stacks,
          FungibleConditionCode.Equal,
          new BN(amountXBTC),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  } else {
    options = {
      contractAddress: SEND_XBTC_MANY_CONTRACT.address,
      contractName: SEND_XBTC_MANY_CONTRACT.name,
      functionName: "send-xbtc-many",
      functionArgs: [
        listCV(
          details.map((d) => {
            return tupleCV({
              to: standardPrincipalCV(d.recipient),
              "xbtc-in-sats": uintCV(d.amountXBTC),
              memo: bufferCVFromString(memo),
              "swap-to-ustx": falseCV(),
              "min-dy": noneCV(),
            });
          })
        ),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          poolStxFoundation.stacks,
          FungibleConditionCode.Equal,
          new BN(total),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  }
  const tx = await makeContractCall({
    ...options,
    senderKey: poolStxFoundation.private,
    sponsored: sponsored,
    fee: new BN(0),
    nonce: sponsored
      ? nonce
        ? new BN(nonce)
        : undefined
      : new BN(sponsorNonce),
    network,
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    if (sponsored) {
      const sponsoredTx = await sponsorTransaction({
        transaction: tx,
        fee: new BN(payoutFee),
        sponsorPrivateKey: poolAdmin.private,
        sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
      });
      try {
        const result = await handleTransaction(sponsoredTx);
        console.log({ result, sponsorNonce, nonce });
      } catch (e: any) {
        console.log("err", e.toString(), e.toString().includes("BadNonce"));
        if (
          e.toString().includes("BadNonce") ||
          e.toString().includes("ConflictingNonceInMempool")
        ) {
          return;
        } else {
          throw e;
        }
      }
    } else {
      const result = await handleTransaction(tx);
      console.log({ result, nonce: sponsorNonce }); // paid by pool admin
    }
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: poolStxFoundation.stacks,
        arguments: options.functionArgs.map((a) => cvToHex(a)),
      }),
    });
    console.log(
      { result },
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

(async () => {
  const distributions = readFileSync(`./tool-scripts/${filename}`).toString();
  const lines = distributions.split("\n");
  console.log(lines);

  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolStxFoundation.stacks,
    proof: 0,
  });
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });

  const { nonce, sponsorNonce } = firstChaining
    ? { nonce: accountInfo.nonce, sponsorNonce: sponsorAccountInfo.nonce }
    : noncesForRepeatedChaining;
  console.log({ nonce, sponsorNonce });

  let i = 0;
  let total = 0;
  let batch = [];

  for (let line of lines) {
    let [name, amountString] = line.split(",");
    name = name.replace("-btc", ".btc");
    name = name.replace("-stx", ".stx");
    const amount = parseInt(amountString);

    if (amount <= 0) {
      continue;
    }
    try {
      const recipient = await bnsApi.getNameInfo({ name });
      try {
        standardPrincipalCV(recipient.address);
      } catch (e) {
        console.log(e, name, recipient.address);
        continue;
      }
      const details = {
        amountXBTC: amount,
        recipient: recipient.address,
      };
      if (grouped) {
        batch.push(details);
        if (batch.length > 20) {
          await payoutAdvocat(batch, nonce + i, sponsorNonce + i);
          i += 1;
          batch = [];
        }
      } else {
        await payoutAdvocat([details], nonce + i, sponsorNonce + i);
        i += 1;
      }
      total += amount;
    } catch (e) {
      console.log(JSON.stringify(e));
      console.log({ i, nonce, sponsorNonce });
      break;
    }
  }

  if (batch.length > 0) {
    await payoutAdvocat(batch, nonce + i, sponsorNonce + i);
    i += 1;
    batch = [];
  }

  const balances = await accountsApi.getAccountBalance({
    principal: poolStxFoundation.stacks,
  });
  const treasury =
    parseInt(
      (balances.fungible_tokens as any)[
        "SP3DX3H4FEYZJZ586MFBS25ZW3HZDMEW92260R2PR.Wrapped-Bitcoin::wrapped-bitcoin"
      ].balance
    ) / 100_000_000;
  console.log(
    `Rewards of ${
      total / 100_000_000
    } xBTC have been distributed in ${i} txs from the treasury of ${treasury} xBTC: https://explorer.stacks.co/address/SPSTX06BNGJ2CP1F6WA8V49B6MYD784N6YZMK95G?chain=mainnet`
  );
})();
