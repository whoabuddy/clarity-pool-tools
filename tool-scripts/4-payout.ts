import { ReadOnlyFunctionArgsFromJSON } from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  bufferCV,
  bufferCVFromString,
  callReadOnlyFunction,
  createAssetInfo,
  cvToHex,
  cvToString,
  falseCV,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardFungiblePostCondition,
  makeStandardSTXPostCondition,
  noneCV,
  someCV,
  sponsorTransaction,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import {
  SEND_XBTC_MANY_CONTRACT,
  stackerReplaceMap,
  xbtcReceiver,
  XBTC_CONTRACT,
} from "../src/constants";
import { network, handleTransaction, NEXT_CYCLE } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  accountsApi,
  downloadPoolPayoutHints,
  getPayoutHints,
  getStackersFromBoombox,
  boomboxes,
  downloadBoomboxesTxs,
  downloadPoxTxs,
  getStackersFromPox,
  boomboxes2,
  boombox_admin,
  getStackersFromBoomboxAdmin,
  downloadBoomboxAdminTxs,
  contractsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const BN = require("bn.js");

const {
  poolAdmin,
  pool3cycles,
  pool6cycles,
  pool700,
  poolStxFoundation,
  fpoo1Payout,
} = keys;

const dryrun = false;
const sendManyFee = 50_000;
const payoutFeeXBTCContract = 6_000;
const payoutFeeSendManyContract = 50_000;

const rewardCycle = NEXT_CYCLE - 1; // <---------- edit here
const rewardsBTC = 0.96662884;
const exchangeRate = Math.round(95922000 / 72199.303419); // 1631;

const boomboxAdminId = 17; // <------------------- edit here

let totalRewardsBTC = rewardsBTC;
let totalRewards = (rewardsBTC * 100000000 * 1000000) / exchangeRate;
//totalRewards = 1869635286; // directly from swap

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number,
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) => {
          let receiver = s.stacker;
          if (s.stacker in stackerReplaceMap) {
            receiver = (stackerReplaceMap as any)[s.stacker];
            console.log("replaced by mails", s.stacker, receiver);
          }
          if (s.stacker in payoutHints) {
            const stackerHint = (payoutHints as any)[s.stacker];
            if (stackerHint.recipient) {
              receiver = stackerHint.recipient;
              console.log("replaced by hints", s.stacker, receiver);
            }
          }
          return tupleCV({
            to: standardPrincipalCV(receiver),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from(memo)),
          });
        })
      ),
    ],
    senderKey: fpoo1Payout.private,
    nonce: nonce ? new BN(nonce) : undefined,
    fee: new BN(0),
    network,
    sponsored: true,
    postConditions: [
      makeStandardSTXPostCondition(
        fpoo1Payout.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(sendManyFee),
      sponsorPrivateKey: poolAdmin.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result });
  }
}

async function logAndPayout(
  stackerSet: {
    rewardCycle: number;
    amount: bigint;
    stacker: string;
    reward: number;
  }[],
  nonce: number,
  sponsorNonce: number,
  memo: string,
  payoutHints: object
) {
  const totalPayoutSet = stackerSet.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const totalStackedSet = stackerSet.reduce(
    (sum: bigint, s) => sum + s.amount,
    0n
  );
  console.log({
    stackerSetLength: stackerSet.length,
    totalStackedSet,
    totalPayoutSet,
    nonce,
  });
  await payoutSet(
    stackerSet,
    totalPayoutSet,
    nonce,
    sponsorNonce,
    memo,
    payoutHints
  );
}

async function payoutInXbtc(
  details: { amountXBTC: number; recipient: string }[],
  nonce: number,
  sponsorNonce: number,
  memo: string
) {
  let total = details.reduce((sum, d) => sum + d.amountXBTC, 0);
  let options;
  const useXBTCContract = details.length === 1;
  if (useXBTCContract) {
    const { amountXBTC, recipient } = details[0];
    options = {
      contractAddress: XBTC_CONTRACT.address,
      contractName: XBTC_CONTRACT.name,
      functionName: "transfer",
      functionArgs: [
        uintCV(amountXBTC),
        standardPrincipalCV(fpoo1Payout.stacks),
        standardPrincipalCV(recipient),
        someCV(bufferCVFromString(memo)),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          fpoo1Payout.stacks,
          FungibleConditionCode.Equal,
          new BN(amountXBTC),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  } else {
    options = {
      contractAddress: SEND_XBTC_MANY_CONTRACT.address,
      contractName: SEND_XBTC_MANY_CONTRACT.name,
      functionName: "send-xbtc-many",
      functionArgs: [
        listCV(
          details.map((d) => {
            return tupleCV({
              to: standardPrincipalCV(d.recipient),
              "xbtc-in-sats": uintCV(d.amountXBTC),
              memo: bufferCVFromString(memo),
              "swap-to-ustx": falseCV(),
              "min-dy": noneCV(),
            });
          })
        ),
      ],
      postConditions: [
        makeStandardFungiblePostCondition(
          fpoo1Payout.stacks,
          FungibleConditionCode.Equal,
          new BN(total),
          createAssetInfo(
            XBTC_CONTRACT.address,
            XBTC_CONTRACT.name,
            "wrapped-bitcoin"
          )
        ),
      ],
    };
  }
  const tx = await makeContractCall({
    ...options,
    senderKey: fpoo1Payout.private,
    sponsored: true,
    fee: new BN(0),
    nonce: nonce ? new BN(nonce) : undefined,
    network,

    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(
        useXBTCContract ? payoutFeeXBTCContract : payoutFeeSendManyContract
      ),
      sponsorPrivateKey: poolAdmin.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result, sponsorNonce, nonce });
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: fpoo1Payout.stacks,
        arguments: options.functionArgs.map((a) => cvToHex(a)),
      }),
    });
    console.log(
      { result },
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

const payout = async () => {
  const memo = `reward cycle #${rewardCycle} payout`;

  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadPoxTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadBoomboxAdminTxs(network);
  await downloadPoolPayoutHints(network);
  const payoutHints = getPayoutHints();

  // get stackers from all cycles
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  console.log({ v0StackersLength: v0Stackers.length });
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network,
    poolStxFoundation.stacks // exclude advocate pool members
  );
  console.log({ v1StackersLength: v1Stackers.length });
  const boomboxStackers = getStackersFromBoombox(
    `${boomboxes.address}.${boomboxes.name}`,
    boomboxes.rewardCycle,
    network
  );
  console.log({ countBoomboxes: boomboxStackers.length, name: boomboxes.name });

  const boomboxStackers2 = getStackersFromBoombox(
    `${boomboxes2.address}.${boomboxes2.name}`,
    boomboxes2.rewardCycle,
    network
  );
  console.log({
    countBoomboxes2: boomboxStackers2.length,
    name: boomboxes2.name,
  });

  // stackers from custom boomboxes
  const details: any = await callReadOnlyFunction({
    contractAddress: boombox_admin.address,
    contractName: boombox_admin.name,
    functionName: "get-boombox-by-id",
    functionArgs: [uintCV(boomboxAdminId)],
    senderAddress: boombox_admin.address,
  });

  const boomboxAdminStackers = await getStackersFromBoomboxAdmin(
    boomboxAdminId,
    Number(details.value.data["cycle"].value),
    Number(details.value.data["locking-period"].value),
    network
  );

  console.log({
    boomboxAdminCount: boomboxAdminStackers.length,
    name: cvToString(details.value.data["fq-contract"]),
    cycle: Number(details.value.data["cycle"].value),
    lockingPeriod: Number(details.value.data["locking-period"].value),
  });

  // stacker from pox
  const poxStackers = getStackersFromPox(network, [
    poolAdmin,
    pool3cycles,
    pool6cycles,
    pool700,
  ]);

  console.log({ countPox: poxStackers.length });

  // filter stackers by cycle
  const stackersInCycle = stackersToCycle(
    v0Stackers
      .concat(v1Stackers)
      .concat(boomboxStackers)
      .concat(boomboxStackers2)
      .concat(poxStackers)
      .concat(boomboxAdminStackers),
    rewardCycle
  );

  // filter stackers by cycle and origin
  const boomboxAdminInCycle = stackersToCycle(
    boomboxAdminStackers,
    rewardCycle
  );
  const boomboxesInCycle = stackersToCycle(boomboxStackers, rewardCycle);
  const boomboxesInCycle2 = stackersToCycle(boomboxStackers2, rewardCycle);
  const poxInCycle = stackersToCycle(poxStackers, rewardCycle);

  // update totalStacked
  const stackers = stackersInCycle.members;
  const totalStacked = stackers.reduce(
    (sum: number, s) => sum + Number(s.amount),
    0
  );

  // boombox stats
  const boomboxStacked = boomboxesInCycle.members.reduce(
    (sum: number, s) => sum + Number(s.amount),
    0
  );
  const boomboxStacked2 = boomboxesInCycle2.members.reduce((sum: number, s) => {
    return sum + Number(s.amount);
  }, 0);
  const boomboxAdminStacked = boomboxAdminInCycle.members.reduce(
    (sum: number, s) => {
      return sum + Number(s.amount);
    },
    0
  );

  const boomboxAdminTotalRewardsSATS = Math.floor(
    (totalRewardsBTC * boomboxAdminStacked * 100_000_000) / totalStacked
  );

  // non boombox stacker receive rewards in xbtc
  const xbtcReceiverStackers = stackers
    // ignore boombox stackers
    .filter(
      (s) =>
        boomboxesInCycle.members.findIndex((m) => m.stacker === s.stacker) <
          0 &&
        boomboxesInCycle2.members.findIndex((m) => m.stacker === s.stacker) <
          0 &&
        boomboxAdminInCycle.members.findIndex((m) => m.stacker === s.stacker) <
          0
    )
    // only xbtcReceivers
    .filter(
      (s) => ((xbtcReceiver as any)[rewardCycle] as any)[s.stacker] === true
    );

  const xbtcReceiverStackersStackedAmount = xbtcReceiverStackers.reduce(
    (sum: number, s) => {
      return sum + Number(s.amount);
    },
    0
  );

  const adjustedTotalRewards =
    totalRewards -
    ((boomboxStacked +
      boomboxStacked2 +
      boomboxAdminStacked +
      xbtcReceiverStackersStackedAmount) *
      totalRewards) /
      totalStacked;

  // extend stacker with reward amount and remove stackers that are paid out differently
  const filteredExtendedStackers = stackers
    // don't payout boombox stackers now
    .filter(
      (s) =>
        boomboxesInCycle.members.findIndex((m) => m.stacker === s.stacker) <
          0 &&
        boomboxesInCycle2.members.findIndex((m) => m.stacker === s.stacker) <
          0 &&
        boomboxAdminInCycle.members.findIndex((m) => m.stacker === s.stacker) <
          0
    )
    // don't payout xbtc receivers
    .filter(
      (s) => ((xbtcReceiver as any)[rewardCycle] as any)[s.stacker] !== true
    )
    // calculate reward
    .map((stacker) => {
      // reward
      const reward = Math.round(
        (Number(stacker.amount) * totalRewards) / totalStacked
      );
      return {
        reward,
        ...stacker,
      };
    })
    // don't try to payout rewards === 0
    .filter((s) => s.reward > 0);

  const totalPayout = filteredExtendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  const xbtcReceiverPayout =
    (xbtcReceiverStackersStackedAmount * totalRewards) / totalStacked;
  const xbtcReceiverXbtc =
    (xbtcReceiverStackersStackedAmount * totalRewardsBTC) / totalStacked;
  console.log({
    totalStacked,
    totalRewards,
    percentage: totalRewards / totalStacked,
    numberOfStackers: stackers.length,
    numberOfStackersToPay: filteredExtendedStackers.length,
    adjustedTotalRewards,
    totalPayout,
    boomboxStacked,
    numberOfBoomboxers: boomboxesInCycle.members.length,
    boomboxPayout: (boomboxStacked * totalRewards) / totalStacked,
    boomboxStacked2,
    numberOfBoomboxers2: boomboxesInCycle2.members.length,
    boombox2Payout: (boomboxStacked2 * totalRewards) / totalStacked,
    boomboxAdminStacked,
    numberOfBoomboxAdmin: boomboxAdminInCycle.members.length,
    boomboxAdminPayout: (boomboxAdminStacked * totalRewards) / totalStacked,
    boomboxAdminTotalRewardsSATS,
    numberOfxbtcReceivers: xbtcReceiverStackers.length,
    xbtcReceiverPayout: xbtcReceiverPayout.toFixed(6),
    xbtcReceiverXbtc: xbtcReceiverXbtc.toFixed(8),
    xbtcToConvert: (totalRewardsBTC - xbtcReceiverXbtc).toFixed(8),
    exchangeRate,
  });

  // get nonce
  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  let sponsorNonce = sponsorAccountInfo.nonce;
  console.log({ nonce, sponsorNonce });

  //
  // payout in stx
  //

  // number of tx = mod 200 + 1 tx for the remainder
  let numberOfSendManyTxs =
    Math.floor(filteredExtendedStackers.length / 200) + 1;
  for (let i = 0; i < numberOfSendManyTxs; i++) {
    const set = filteredExtendedStackers.slice(i * 200, (i + 1) * 200);
    await logAndPayout(set, nonce + i, sponsorNonce + i, memo, payoutHints);
  }

  //
  // payout in xbtc
  //
  let numberOfSendManyxBTCTxs =
    Math.floor(xbtcReceiverStackers.length / 20) + 1;
  for (let i = 0; i < numberOfSendManyxBTCTxs; i++) {
    const set = xbtcReceiverStackers.slice(i * 20, (i + 1) * 20).map((s) => {
      return {
        amountXBTC: Math.floor(
          (Number(s.amount) * totalRewardsBTC * 100_000_000) / totalStacked
        ),
        recipient: s.stacker,
      };
    });
    await payoutInXbtc(
      set,
      nonce + numberOfSendManyTxs + i,
      sponsorNonce + numberOfSendManyTxs + i,
      memo
    );
  }
};

payout();
