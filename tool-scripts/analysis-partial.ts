import { NEXT_CYCLE } from "../src/deploy";
import { getPartialStacked } from "../src/pool-tool-partials";
import { boombox_admin } from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolStxFoundation } = keys;
const btcAddrFriedgerPool = "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq";
const btcAddrStx = "38dSq9XfVhu51PH51Rpsirr4XgymikdGu1";
const btcAddrDaemonTech = "38Jht2bzmJL4EwoFvvyFzejhfEb4J7KxLb";
const btcAddrCharity = "3MxgufNKFWeeMcsHHnotyjcwqRbsNu3bGv";

function addPartial(partial: any, total: number) {
  if (partial) {
    if (partial.amount === "none") {
      console.log(partial.cycle, partial.poolAdmin, partial.btcAddress, "none");
    } else {
      const amount = parseInt(partial.amount);
      total += amount;
      console.log(
        partial.cycle,
        partial.poolAdmin,
        partial.btcAddress,
        (amount / 1000000).toLocaleString(undefined, {
          style: "currency",
          currency: "STX",
        })
      );
    }
  }
  return total;
}

(async () => {
  const cycleId = NEXT_CYCLE;
  let total = 0;

  for (let adminName of [...Object.keys(keys)]) {
    const admin = (keys as any)[adminName];
    const partial = await getPartialStacked(
      admin.stacks,
      cycleId,
      admin.stacks === poolStxFoundation.stacks
        ? btcAddrStx
        : btcAddrFriedgerPool
    );
    total = addPartial(partial, total);
  }

  // the surfer kids
  let partial = await getPartialStacked(
    `${boombox_admin.address}.${boombox_admin.name}`,
    cycleId,
    btcAddrCharity
  );
  addPartial(partial, total);

  // Friedger Pool - Boombox Admin
  partial = await getPartialStacked(
    `${boombox_admin.address}.${boombox_admin.name}`,
    cycleId,
    btcAddrFriedgerPool
  );
  total = addPartial(partial, total);

  // daemon tech
  partial = await getPartialStacked(
    "SP1MXS1JGD99FMAVGFWDMVBTTKVRH9RJN0MN0EKJ7",
    cycleId,
    btcAddrDaemonTech
  );
  addPartial(partial, total);

  // brink
  partial = await getPartialStacked(
    "SPXVRSEH2BKSXAEJ00F1BY562P45D5ERPSKR4Q33",
    cycleId,
    "1MKrNDn9kZSBWpCvYwQr4ZkiR6rcMm2qF7"
  );
  addPartial(partial, total);

  console.log("===================================");
  console.log(
    "total:",
    (total / 1000000).toLocaleString(undefined, {
      style: "currency",
      currency: "STX",
    })
  );
})();
