import { cvToString, standardPrincipalCV, uintCV } from "@stacks/transactions";
import { handleTransaction, mainnet } from "../src/deploy";
import {
  accountsApi,
  cycleLength,
  firstBurnChainBlock,
  infoApi,
  makePoxDelegateStackStxCall,
  stackDelegatedStxsInBatches,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BigNum from "bn.js";

const { poolStxFoundation } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "38dSq9XfVhu51PH51Rpsirr4XgymikdGu1"
);

(async () => {
  const cycleId = mainnet ? 38 : 124;
  const lockingPeriod = 12;
  const admin = poolStxFoundation;
  // change here end
  const info = await infoApi.getCoreApiInfo();
  console.log(info.burn_block_height);
  
  const startBlock = firstBurnChainBlock + cycleId * cycleLength - 100; // start block of next cycle
  const startBurnHeight = startBlock - 150;

  const stackerCV = standardPrincipalCV(
    "SP132XA5EFZG9FP31PAGWEE6AP0E6PJMJ4DJARXC5"
  );
  const amountUstxCV = uintCV(new BigNum(1_000_000_000_000).toString(10));

  let accountInfo = await accountsApi.getAccountInfo({
    principal: admin.stacks,
    proof: 0,
  });
  const nonce = accountInfo.nonce;
  console.log({ nonce, admin: admin.stacks });

  const tx = await makePoxDelegateStackStxCall({
    stackerCV,
    amountUstxCV,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    poolAdmin: admin,
    nonce,
  });
  const result = await handleTransaction(tx);
  console.log(result);
})();
