import { network } from "../src/deploy";
import {
  accountsApi,
  downloadAccountTxs,
  logAccountTxsCSV,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, pool700, poolStxFoundation } =
  keys;

const admins = [
  poolAdmin,
  pool3cycles,
  pool6cycles,
  pool700,
  poolStxFoundation,
];

(async () => {
  for (let p of admins) {
    const response = await accountsApi.getAccountInfo({ principal: p.stacks });
    console.log({ p: p.stacks, nonce: response.nonce });
  }
})();
