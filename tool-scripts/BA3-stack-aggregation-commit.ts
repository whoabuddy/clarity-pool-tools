import {
  AnchorMode,
  bufferCVFromString,
  makeContractCall,
  SignedContractCallOptions,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { handleTransaction, network, NEXT_CYCLE } from "../src/deploy";
import {
  accountsApi,
  boomboxes,
  poxContractAddress,
} from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";

const { poolAdmin } = keys;

const pools = [
  {
    rewardPoxAddrCV: poxAddrCVFromBitcoin("33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"),
  },
];

(async () => {
  const cycleId = NEXT_CYCLE;   // <--- edit here

  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  for (let p of pools) {
    const admin = p.rewardPoxAddrCV;
    const options: SignedContractCallOptions = {
      contractAddress: "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
      contractName: "boombox-admin-v3",
      functionName: "stack-aggregation-commit",
      functionArgs: [p.rewardPoxAddrCV, uintCV(cycleId)],
      senderKey: poolAdmin.private,
      network: network,
      fee: new BN(7000),
      nonce: new BN(nonce),
      anchorMode: AnchorMode.Any,
    };
    const tx = await makeContractCall(options);

    await handleTransaction(tx);
    nonce += 1;
  }
})();
