import { readFileSync } from "fs";
import { network, NEXT_CYCLE } from "../src/deploy";
import {
  downloadPoxTxs,
  writeDelegationStates,
  DelegationDataCV,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const pools = [
  { admin: poolAdmin },
  { admin: pool3cycles },
  { admin: pool6cycles },
  { admin: pool700 },
];

const cycleToExcludeMembers = NEXT_CYCLE - 1;
async function writeDelegationAllStates() {
  const fromBlock = undefined; //76900; // <-- edit here
  const locked: any[] = JSON.parse(
    readFileSync(`members-${cycleToExcludeMembers}.json`).toString()
  ).members;
  const states: {
    stacker: string;
    data: DelegationDataCV | undefined;
    status: any;
  }[] = [];
  const txs = await downloadPoxTxs(network);

  for (let p of pools) {
    await writeDelegationStates(txs, p.admin.stacks, locked, states, fromBlock);
    console.log({ statesSoFar: states.length });
  }
}

(async () => {
  await writeDelegationAllStates();
})();
