import { network } from "../src/deploy";
import { downloadAccountTxs, logAccountTxsCSV } from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin } = keys;

(async () => {
  await downloadAccountTxs(poolAdmin.stacks, network);
  await logAccountTxsCSV(poolAdmin.stacks, network);
})();
