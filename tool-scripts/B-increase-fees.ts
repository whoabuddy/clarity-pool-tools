import { ReadOnlyFunctionArgsFromJSON } from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  cvToHex,
  cvToString,
  FungibleConditionCode,
  hexToCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  PostConditionMode,
  sponsorTransaction,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import { contractsApi } from "../src/pool-tool-utils";
import { keys } from "./config";
import { readFileSync } from "fs";

const BN = require("bn.js");

const { poolAdmin, fpoo1Payout } = keys;

const dryrun = false;
const sponsored = true;
const payStacksTipCV = uintCV(39_000); // stacks height for payout
const payoutFee = new BN(1_001_000);

async function payoutSet(
  mempoolTx: any,
  sender: { private: string; stacks: string },
  sponsor: { private: string },
  sponsorNonce: number
) {
  const rewardUstxCV = hexToCV(mempoolTx.contract_call.function_args[0].hex);
  const [contractAddress, contractName] =
    mempoolTx.contract_call.contract_id.split(".");
  const functionName = mempoolTx.contract_call.function_name;
  const functionArgs = mempoolTx.contract_call.function_args.map((a: any) =>
    hexToCV(a.hex)
  );
  const postConditionMode =
    mempoolTx.post_condition_mode === "allow"
      ? PostConditionMode.Allow
      : PostConditionMode.Deny;
  const postConditions = mempoolTx.post_conditions.map((pc) => {});
  const nonce = new BN(mempoolTx.nonce);
  const options = {
    contractAddress,
    contractName,
    functionName,
    functionArgs,
  };
  const tx = await makeContractCall({
    ...options,
    senderKey: sender.private,
    sponsored: sponsored,
    fee: new BN(payoutFee),
    nonce,
    network,
    postConditionMode,
    postConditions: [
      makeStandardSTXPostCondition(
        sender.stacks,
        FungibleConditionCode.LessEqual,
        new BN((rewardUstxCV as any).value.toNumber() + 1_000_000)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(payoutFee),
      sponsorPrivateKey: sponsor.private,
      sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result, sponsorNonce, nonce });
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: poolAdmin.stacks,
        arguments: options.functionArgs.map((a: any) => cvToHex(a)),
      }),
    });
    console.log({ result });
    console.log(
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

async function nop(
  mempoolTx: any,
  sender: { private: string; stacks: string },
  sponsor: { private: string },
  sponsorNonce: number
) {
  const [contractAddress, contractName] =
    "SP3D6PV2ACBPEKYJTCMH7HEN02KP87QSP8KTEH335.commission-nop".split(".");
  const functionName = "pay";
  const functionArgs = [uintCV(0), uintCV(0)];
  const options = {
    contractAddress,
    contractName,
    functionName,
    functionArgs,
  };
  const tx = await makeContractCall({
    ...options,
    senderKey: sender.private,
    sponsored: sponsored,
    fee: new BN(1_001_000),
    nonce: new BN(mempoolTx.nonce),
    network,
    postConditionMode: PostConditionMode.Deny,
    postConditions: [],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    const sponsoredTx = await sponsorTransaction({
      transaction: tx,
      fee: new BN(1_001_000),
      sponsorPrivateKey: sponsor.private,
      sponsorNonce: new BN(sponsorNonce),
    });
    const result = await handleTransaction(sponsoredTx);
    console.log({ result, sponsorNonce, nonce: mempoolTx.nonce });
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: poolAdmin.stacks,
        arguments: options.functionArgs.map((a) => cvToHex(a)),
      }),
    });
    console.log({ result });
    console.log(
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

(async () => {
  let txs = JSON.parse(
    readFileSync("mempool-txs-1-2021-11-26T09:27:31.294Z.json").toString()
  );
  txs = txs.filter(
    (tx: any) =>
      tx.sender_address === "SPFP0018FJFD82X3KCKZRGJQZWRCV9793QTGE87M" &&
      tx.sponsor_address === "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60"
  );
  const txsByNonce: any = {};
  txs.map((tx: any) => (txsByNonce[tx.nonce] = tx));
  txsByNonce[37] = txsByNonce[38];
  txsByNonce[37].nonce = 37;
  txsByNonce[38] = {
    sender_address: "SPFP0018FJFD82X3KCKZRGJQZWRCV9793QTGE87M",
    sponsor_address: "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
    nonce: 38,
  };
  await payoutSet(txsByNonce["33"], fpoo1Payout, poolAdmin, 2098);
  await payoutSet(txsByNonce["34"], fpoo1Payout, poolAdmin, 2099);
  await payoutSet(txsByNonce["35"], fpoo1Payout, poolAdmin, 2100);
  await payoutSet(txsByNonce["36"], fpoo1Payout, poolAdmin, 2101);
  await payoutSet(txsByNonce["37"], fpoo1Payout, poolAdmin, 2102);
  await nop(txsByNonce["38"], fpoo1Payout, poolAdmin, 2103);
})();
