import { hexToCV } from "@stacks/transactions";
import { network } from "../src/deploy";
import { getFPBtcTxs } from "../src/fpwr-utils";
import { downloadFPWRTxs } from "../src/pool-tool-utils";

(async () => {
  await downloadFPWRTxs(network);
  const { txs } = await getFPBtcTxs();
  const result = txs.reduce(
    (result: { ustx: number; sats: number }, tx: any) => {
      const { ustx, value } = (hexToCV(tx.tx_result.hex) as any).value.data;
      return {
        ustx: result.ustx + ustx.value.toNumber(),
        sats: result.sats + value.value.toNumber(),
      };
    },
    { ustx: 0, sats: 0 }
  );
  console.log({ txs: txs.length, result });
})();
