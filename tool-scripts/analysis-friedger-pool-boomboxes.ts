import { writeFileSync } from "fs";
import { network } from "../src/deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
  downloadBoomboxesTxs,
  boomboxes,
  getStackersFromBoombox,
  downloadPoxTxs,
  getStackersFromPox,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, pool700 } = keys;
(async () => {
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  await downloadBoomboxesTxs(network);
  await downloadPoxTxs(network);

  const bb = boomboxes
  const cycleId = bb.rewardCycle;
  const boomboxStackers = getStackersFromBoombox(
    `${bb.address}.${bb.name}`,
    cycleId,
    network
  );
  console.log({ boomboxCount: boomboxStackers.length });
  const stackers = boomboxStackers;

  const stackersInCycle = stackersToCycle(stackers, cycleId);
  writeFileSync(`members-bb-${cycleId}.json`, JSON.stringify(stackersInCycle));
})();
