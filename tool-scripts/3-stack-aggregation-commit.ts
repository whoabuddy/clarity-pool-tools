import {
  AnchorMode,
  bufferCVFromString,
  makeContractCall,
  SignedContractCallOptions,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { handleTransaction, network, NEXT_CYCLE } from "../src/deploy";
import { boomboxes, poxContractAddress } from "../src/pool-tool-utils";
import { poxAddrCVFromBitcoin } from "../src/utils-pox-addr";
import { keys } from "./config";
import BN from "bn.js";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

const rewardPoxAddrCV = poxAddrCVFromBitcoin(
  "33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq"
  //"1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF"
);

const pools = [
  { admin: poolAdmin, nonce: 0 },
  { admin: pool3cycles, nonce: 0 },
  { admin: pool6cycles, nonce: 0 },
  { admin: pool700, nonce: 0 },
];

(async () => {
  const cycleId = NEXT_CYCLE; // <--- edit here
  for (let p of pools) {
    const admin = p.admin;
    const options: SignedContractCallOptions = {
      contractAddress: poxContractAddress,
      contractName: "pox",
      functionName: "stack-aggregation-commit",
      functionArgs: [rewardPoxAddrCV, uintCV(cycleId)],
      senderKey: admin.private,
      network: network,
      fee: new BN(1000),
      anchorMode: AnchorMode.Any,
    };
    if (p.nonce > 0) {
      options.nonce = new BN(p.nonce);
    }
    const tx = await makeContractCall(options);

    await handleTransaction(tx);
  }
})();
