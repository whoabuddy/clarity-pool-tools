import { network } from "../src/deploy";
import { downloadAccountTxs, logFTTxsAccountCSV } from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin } = keys;
(async () => {
  await downloadAccountTxs(poolAdmin.stacks, network);
  logFTTxsAccountCSV(poolAdmin.stacks);
})();
