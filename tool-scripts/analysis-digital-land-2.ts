import {
  callReadOnlyFunction,
  ClarityType,
  contractPrincipalCVFromStandard,
  cvToHex,
  cvToJSON,
  cvToString,
  hexToCV,
  SomeCV,
  StringAsciiCV,
  TupleCV,
  tupleCV,
  uintCV,
  UIntCV,
} from "@stacks/transactions";

import * as pg from "pg";
import { network } from "../src/deploy";
import { contractsApi } from "../src/pool-tool-utils";

const analyse = async () => {
  const client = new pg.Client({
    port: 5555,
    user: "postgres",
    password: "postgres",
  });
  await client.connect();

  console.log("[out:json];");
  console.log("(");
  const landIds = await client.query(
    `select value from nft_events ne 
        where 
        asset_identifier = 'SP213KNHB5QD308TEESY1ZMX1BP8EZDPG4JWD0MEA.web4::digital-land'
        and microblock_canonical and canonical`
  );

  for (let land of landIds.rows) {
    const nftIdCV = hexToCV(land.value.toString("hex"));
   
    try {
      const entry = await contractsApi.getContractDataMapEntry({
        contractAddress: "SP213KNHB5QD308TEESY1ZMX1BP8EZDPG4JWD0MEA",
        contractName: "web4",
        mapName: "transfer-utility",
        key: cvToHex(tupleCV({ id: nftIdCV })),
        proof: 0,
      });
      const details = (hexToCV(entry.data) as SomeCV).value as TupleCV;
      console.log(
        `${(details.data.type as StringAsciiCV).data}(${(
          details.data["osm-id"] as UIntCV
        ).value.toString(10)});`
      );
    } catch (e) {
      console.log(cvToString(nftIdCV), e);
    }
  }
  console.log(");");
  console.log("out ids center;");
  await client.end();
};

analyse();
