import { AnchorMode, makeSTXTokenTransfer } from "@stacks/transactions";
import BN from "bn.js";
import { handleTransaction, network } from "../src/deploy";
import { keys } from "./config";

const sender = keys.wallet2;
const nonce = new BN(54);
const fee = new BN(100000);
const recipient = keys.fpoo1Payout.stacks;

console.log(recipient);

(async () => {
  const transaction = await makeSTXTokenTransfer({
    recipient,
    amount: new BN(51376659898),
    senderKey: sender.private,
    network: network,
    nonce: nonce,
    fee: fee,
    memo: "Rewards #24",
    anchorMode: AnchorMode.Any,
  });
  return handleTransaction(transaction);
})();
