import { network } from "../src/deploy";
import {
  downloadPoxTxs,
  logDelegationStatesCSV,
  writeDelegationStates,
} from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolStxFoundation } = keys;

async function writeDelegationAllStates() {
  const fromBlock = 14350;
  const txs = await downloadPoxTxs(network);
  await writeDelegationStates(txs, poolStxFoundation.stacks, [], [], fromBlock);
}

(async () => {
  await writeDelegationAllStates();
  console.log("stacker, stacked, amount, admin, untilBurnHt");
  await logDelegationStatesCSV(poolStxFoundation);
})();
