import { AnchorMode, makeContractCall, uintCV } from "@stacks/transactions";
import { FPWR_04_CONTRACT } from "../src/constants";
import { handleTransaction, network } from "../src/deploy";
import { keys } from "./config";
import BN from "bn.js";

const { poolAdmin } = keys;

async function mint(height: number, amount: number, nonce?: number) {
  const tx = await makeContractCall({
    contractAddress: FPWR_04_CONTRACT.address,
    contractName: FPWR_04_CONTRACT.name,
    functionName: "mint",
    functionArgs: [uintCV(height), uintCV(amount)],
    senderKey: poolAdmin.private,
    network: network,
    postConditions: [],
    nonce: nonce ? new BN(nonce) : undefined,
    anchorMode: AnchorMode.Any,
  });

  const result = await handleTransaction(tx);
  console.log({ result });
}

(async () => {
    mint(27145, 186472162, 443)
})()