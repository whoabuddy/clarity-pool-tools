import { downloadMiamicoinTxs, logCitycoinCSV, logCitycoinTokenCSV } from "../src/citycoins";

(async () => {
    await downloadMiamicoinTxs();
    await logCitycoinTokenCSV();
  })();