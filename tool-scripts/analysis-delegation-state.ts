import { network } from "../src/deploy";
import { logDelegationStatesCSV } from "../src/pool-tool-utils";
import { keys } from "./config";

const { poolAdmin, pool3cycles, pool6cycles, poolCcycles, pool700 } = keys;

(async () => {
  console.log("stacker, stacked, amount, admin, untilBurnHt, untilCycle");
  await logDelegationStatesCSV(poolAdmin);
  await logDelegationStatesCSV(pool3cycles);
  await logDelegationStatesCSV(pool6cycles);
  await logDelegationStatesCSV(pool700);
})();
