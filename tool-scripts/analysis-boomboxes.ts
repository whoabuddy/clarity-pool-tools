import {
  callReadOnlyFunction,
  contractPrincipalCV,
  cvToString,
  uintCV,
} from "@stacks/transactions";
import { network } from "../src/deploy";
import {
  logBoomboxesContractCSV,
  downloadBoomboxesTxs,
} from "../src/pool-tool-utils";

(async () => {
  await downloadBoomboxesTxs(network);
  await logBoomboxesContractCSV(network);

  const result = await callReadOnlyFunction({
    contractAddress: "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
    contractName: "boombox-admin-v3",
    functionName: "nft-details-at-block",
    functionArgs: [
      uintCV(4),
      contractPrincipalCV(
        "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
        "boomboxes-cycle-26"
      ),
      uintCV(27),
      uintCV(46453),
    ],
    senderAddress: "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
  });
  console.log(cvToString(result));
})();
