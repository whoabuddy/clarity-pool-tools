import {
  AccountsApi,
  ReadOnlyFunctionArgsFromJSON,
} from "@stacks/blockchain-api-client";
import {
  AnchorMode,
  callReadOnlyFunction,
  cvToHex,
  cvToString,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  sponsorTransaction,
  uintCV,
} from "@stacks/transactions";
import { network, handleTransaction } from "../src/deploy";
import {
  accountsApi,
  boomboxes,
  boomboxes2,
  contractsApi,
} from "../src/pool-tool-utils";
import { keys } from "./config";
import * as pg from "pg";
import { assert } from "console";

const BN = require("bn.js");

const { poolAdmin, fpoo1Payout } = keys;

const dryrun = true;
const sponsored = true;
const payStacksTipCV = uintCV(42845); // stacks height for payout
const payoutFee = new BN(500_000);

async function payoutSet(
  nftIds: number[],
  totalPayoutOfSet: number,
  nonce: number,
  sponsorNonce: number,
  bb: { address: string; name: string }
) {
  if (nftIds.length === 0) {
    return;
  }
  const rewardUstxCV = uintCV(totalPayoutOfSet);
  const options = {
    contractAddress: bb.address,
    contractName: bb.name,
    functionName: "payout",
    functionArgs: [
      rewardUstxCV,
      listCV(
        nftIds.map((id) => {
          return uintCV(id);
        })
      ),
      payStacksTipCV,
    ],
  };
  const tx = await makeContractCall({
    ...options,
    senderKey: sponsored ? fpoo1Payout.private : poolAdmin.private,
    sponsored: sponsored,
    fee: new BN(0),
    nonce: sponsored
      ? nonce
        ? new BN(nonce)
        : undefined
      : new BN(sponsorNonce),
    network,
    postConditions: [
      makeStandardSTXPostCondition(
        sponsored ? fpoo1Payout.stacks : poolAdmin.stacks,
        FungibleConditionCode.LessEqual,
        new BN(totalPayoutOfSet + 1_000_000)
      ),
    ],
    anchorMode: AnchorMode.Any,
  });

  if (!dryrun) {
    if (sponsored) {
      const sponsoredTx = await sponsorTransaction({
        transaction: tx,
        fee: new BN(payoutFee),
        sponsorPrivateKey: poolAdmin.private,
        sponsorNonce: sponsorNonce ? new BN(sponsorNonce) : undefined,
      });
      const result = await handleTransaction(sponsoredTx);
      console.log({ result, sponsorNonce, nonce });
    } else {
      const result = await handleTransaction(tx);
      console.log({ result, nonce: sponsorNonce }); // paid by pool admin
    }
  } else {
    const result = await contractsApi.callReadOnlyFunction({
      contractAddress: options.contractAddress,
      contractName: options.contractName,
      functionName: options.functionName,
      readOnlyFunctionArgs: ReadOnlyFunctionArgsFromJSON({
        sender: poolAdmin.stacks,
        arguments: options.functionArgs.map((a) => cvToHex(a)),
      }),
    });
    console.log({ result });
    console.log(
      (tx.payload as any).functionArgs.map((a: any) => cvToString(a))
    );
  }
}

(async () => {
  /*
  const client = new pg.Client({
    port: 5555,
    user: "postgres",
    password: "postgres",
  });
  await client.connect();
*/
  // TODO set boombox contract
  const bb = boomboxes;

  /*
  const numberOfBoomboxes = await client
    .query(
      `select count(*) from txs tx 
        where contract_call_contract_id = '${bb.address}.${bb.name}'
        and contract_call_function_name = 'delegate-stx' 
        and microblock_canonical = true
        and canonical = true 
        and status = 1`
    )
    .then((r) => parseInt(r.rows[0].count));
    */

  const numberOfBoomboxes = 185;

  console.log({ numberOfBoomboxes });
  // copied from 4-payout.ts
  const rewardCycle = 22;
  const rewardsBTC = 2.86616811;
  const exchangeRate = 4775;
  const rewardsBTC2 = 0;
  const exchangeRate2 = 1;
  const totalRewards =
    (rewardsBTC * 100000000 * 1000000) / exchangeRate +
    (rewardsBTC2 * 100000000 * 1000000) / exchangeRate2;

  const totalStacked = 17332124017210; // from members-x.json

  assert(
    rewardCycle === bb.rewardCycle,
    "Wrong reward cycle or boombox contract"
  );
  let nftIds = [...Array(numberOfBoomboxes).keys()].map((i) => i + 1);

  let accountInfo = await accountsApi.getAccountInfo({
    principal: fpoo1Payout.stacks,
    proof: 0,
  });
  let nonce = accountInfo.nonce;
  nonce = 56;
  let sponsorAccountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  let sponsorNonce = sponsorAccountInfo.nonce;
  sponsorNonce = 2263;
  let boomboxStackedTotal = 0;
  let boomboxRewardsTotal = 0;
  let bbPerBatch = 80;
  console.log(nftIds.length / bbPerBatch);
  const stackedAmounts = [66061100000, 305047000214, 7654600000];
  console.log("diff", 378762700214 - stackedAmounts.reduce((i, s) => s + i, 0));
  for (let i = 0; i < nftIds.length / bbPerBatch; i++) {
    const lowerBound = i * bbPerBatch;
    const upperBound = lowerBound + bbPerBatch;
    console.log(lowerBound, upperBound, stackedAmounts[0]);
    const set = nftIds
      .filter((id) => id > lowerBound && id <= upperBound)
      .slice(0, 750);
    /* let r = await client.query(
      `select sum(sle.locked_amount) from stx_lock_events sle 
      join nft_events ne on sle.tx_id = ne.tx_id  
      join txs t on t.tx_id = ne.tx_id 
      where t.canonical = true and t.microblock_canonical = true and
      sle.canonical = true and sle.microblock_canonical = true and 
      ne.canonical = true and ne.microblock_canonical = true and
      ('x' || right(ne.value::text, 6))::bit(24)::int  > ${lowerBound} and
      ('x' || right(ne.value::text, 6))::bit(24)::int  <= ${upperBound} and
      t.contract_call_contract_id  = '${bb.address}.${bb.name}'`
    );
    const boomboxStacked = parseInt(r.rows[0].sum);
    */
    const boomboxStacked = stackedAmounts[i];
    const boomboxRewards = (boomboxStacked * totalRewards) / totalStacked;
    boomboxStackedTotal += boomboxStacked;
    boomboxRewardsTotal += boomboxRewards;
    console.log({
      numberOfBoomboxes: set.length,
      boomboxStacked,
      boomboxRewards,
    });
    await payoutSet(set, boomboxRewards, nonce + i, sponsorNonce + i, bb);
  }
  console.log({ boomboxRewardsTotal, boomboxStackedTotal });
  //await client.end();
})();
