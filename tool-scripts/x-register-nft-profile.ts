import {
  AnchorMode,
  contractPrincipalCV,
  makeContractCall,
  uintCV,
} from "@stacks/transactions";
import BN from "bn.js";
import { handleTransaction, network } from "../src/deploy";

const fee = new BN(2_500);
const senderKey =
  "private key";

(async () => {
  const transaction = await makeContractCall({
    contractAddress: "SP2PABAF9FTAJYNFZH93XENAJ8FVY99RRM50D2JG9",
    contractName: "owned-profiles-v1",
    functionName: "register",
    functionArgs: [
      contractPrincipalCV(
        "SP1QK1AZ24R132C0D84EEQ8Y2JDHARDR58R72E1ZW",
        "boomboxes-cycle-34"
      ),
      uintCV(47),
      contractPrincipalCV(
        "SP2PABAF9FTAJYNFZH93XENAJ8FVY99RRM50D2JG9",
        "owned-profiles-v1-commission-nop"
      ),
    ],
    senderKey,
    network: network,
    fee: fee,
    anchorMode: AnchorMode.Any,
  });
  return handleTransaction(transaction);
})();
