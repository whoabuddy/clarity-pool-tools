import {
  AnchorMode,
  broadcastTransaction,
  ClarityType,
  cvToString,
  hexToCV,
  makeContractCall,
  PostConditionMode,
  TxBroadcastResult,
  TxBroadcastResultOk,
  TxBroadcastResultRejected,
  TxRejectedReason,
} from "@stacks/transactions";
import { readFileSync, writeFileSync } from "fs";
import {
  ERR_API_FAILURE,
  ERR_DIFFERENT_HEX,
  ERR_NO_STACKS_BLOCK,
  paramsFromTx,
  wasTxMined,
} from "../src/btcTransactions";
import { FPWR_03_CONTRACT } from "../src/constants";
import { network, user } from "../src/deploy";
import { filenameByBtcTxId, getFPBtcTxs } from "../src/fpwr-utils";
import { accountsApi, downloadFPWRTxs } from "../src/pool-tool-utils";
import BN from "bn.js";

const senderKey = user.private;

// edit here
const txFilter = (d: string[]) => true; //d[0].startsWith("f");

async function submitBtcTx(
  btcTxId: string,
  nonce: number
): Promise<TxBroadcastResult> {
  // create all necessary parameters
  const {
    txCV,
    proofCV,
    block,
    blockCV,
    headerPartsCV,
    header,
    headerParts,
    stxHeight,
    txPartsCV,
    error,
  } = await paramsFromTx(btcTxId);

  // check for flash block
  if (
    !stxHeight ||
    !txCV ||
    !proofCV ||
    !block ||
    !blockCV ||
    !headerPartsCV ||
    !header ||
    !headerParts ||
    !stxHeight ||
    !txPartsCV
  ) {
    return error || "";
  }

  // output parameters
  console.log({
    btcTxId,
    stxHeight,
  });

  // verify off-chain
  try {
    const results = await Promise.all([
      /* getReversedTxId(txCV),
      verifyMerkleProof(btcTxId, block, proofCV),
      verifyMerkleProof2(txCV, headerPartsCV, proofCV),
      verifyBlockHeader(headerParts, stxHeight),
      verifyBlockHeader2(blockCV),
      wasTxMinedFromHex(blockCV, txCV, proofCV),
      parseBlockHeader(header),*/
      wasTxMined(headerPartsCV, txCV, proofCV),
    ]);
    console.log({ r: results.map((r) => cvToString(r)) });
    if (results[0].type === ClarityType.ResponseErr) {
      return {
        error: cvToString(results[0]),
        reason: TxRejectedReason.SignatureValidation,
        reason_data: "wasTxMined error",
        txid: btcTxId,
      };
    }
  } catch (e) {
    console.log(e);
    throw e;
  }

  // submit tx on-chain

  const functionArgs = [
    // block
    headerPartsCV,
    // tx
    txPartsCV,
    // proof
    proofCV,
  ];

  try {
    // submit
    const tx = await makeContractCall({
      contractAddress: FPWR_03_CONTRACT.address,
      contractName: FPWR_03_CONTRACT.name,
      functionName: "mint",
      functionArgs,
      postConditionMode: PostConditionMode.Deny,
      postConditions: [],
      network: network,
      anchorMode: AnchorMode.Any,
      senderKey,
      nonce: new BN(nonce),
    });
    const result = await broadcastTransaction(tx, network);
    return result;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

// go through all btc txs of cycle 13 and find the ones not yet submitted.

(async () => {
  try {
    await downloadFPWRTxs(network);
    const { txs, btcTxIds, stxTxIds } = await getFPBtcTxs();
    console.log({
      btcTxIdsLength: Object.keys(btcTxIds).length,
      flashBlockTxs: Object.values(btcTxIds).filter(
        (v) => v === ERR_NO_STACKS_BLOCK
      ).length,
    });
    // handle electrum export
    const content = readFileSync("./fpwr-13.csv").toString();
    const lines = content.split("\n").splice(1);
    const data = lines.map((l) => l.split(","));
    console.log({
      data: data.length,
    });

    // get nonce
    const accInfo = await accountsApi.getAccountInfo({
      principal: user.stacks,
      proof: 0,
    });
    const firstNonce = accInfo.nonce;
    console.log({ firstNonce });

    let count = 0;
    let totalUstx = 0;
    let totalSats = 0;
    let totalFlashBlock = 0;
    let countTx = 0;
    let prices: number[] = [];
    const verifiers: any = {};
    for (let d of data.filter(txFilter)) {
      if (!d[0]) {
        continue;
      }
      countTx += 1;
      const stxId = btcTxIds[`0x${d[0]}`] || btcTxIds[d[0]];
      if (stxId) {
        if (stxId === ERR_NO_STACKS_BLOCK) {
          totalFlashBlock += parseFloat(d[3]) * 100_000_000;
        } else {
          const tx = txs.find((tx: any) => tx.tx_id === stxId);
          verifiers[tx.sender_address] =
            (verifiers[tx.sender_address] || 0) + 1;
          const { ustx, value } = (hexToCV(tx.tx_result.hex) as any).value.data;
          if (
            Math.abs(value.value.toNumber() - parseFloat(d[3]) * 100_000_000) >
            0.01
          ) {
            console.log(
              d[0],
              value.value.toNumber(),
              parseFloat(d[3]) * 10_000_000
            );
          }
          totalUstx += ustx.value.toNumber();
          totalSats += value.value.toNumber();
          console.log(
            value.value.toNumber(),
            ustx.value.toNumber(),
            (value.value.toNumber() / ustx.value.toNumber()) * 1_000_000
          );
          prices.push(
            (value.value.toNumber() / ustx.value.toNumber()) * 1_000_000
          );
        }
      } else {
        console.log(stxId, d[0]);
        // try to submit btc reward tx
        try {
          const stxTxId = await submitBtcTx(d[0], firstNonce + count);
          console.log({ nonce: firstNonce + count, stxTxId });
          if (stxTxId === ERR_NO_STACKS_BLOCK) {
            btcTxIds[d[0]] = ERR_NO_STACKS_BLOCK;
            writeFileSync(filenameByBtcTxId, JSON.stringify(btcTxIds));
          }
          if (
            stxTxId !== ERR_NO_STACKS_BLOCK &&
            stxTxId !== ERR_DIFFERENT_HEX &&
            stxTxId !== ERR_API_FAILURE &&
            !(stxTxId as TxBroadcastResultRejected).error
          ) {
            count += 1;
            if (count > 25) {
              console.log("25 btc txs submitted");
              return;
            }
          }
        } catch (e) {
          console.log(e, d[0]);
          return;
        }
      }
    }
    console.log({ countTx, totalFlashBlock, totalSats, totalUstx });
    console.log(prices.reduce((sum, v) => sum + v) / prices.length);
    delete verifiers["SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60"];
    delete verifiers["SPWE4ZR7WHPG80DZ49RBPWMYK9Q8VCJW6PV8XDNQ"];
    const rewardedTxs = Object.entries(verifiers).reduce(
      (total, e) => total + (e[1] as number),
      0
    );
    const payout = Object.entries(verifiers).map((e) => {
      return { stacker: e[0], reward: Math.floor(((e[1] as number) / rewardedTxs) * 500000000) };
      //return { stacker: e[0], reward: Math.floor(((e[1] as number) / rewardedTxs) * 168265349 * 5 / 100) };
    });
    console.log({ verifiers, rewardedTxs, payout });
  } catch (e) {
    console.log(e);
  }
})();
