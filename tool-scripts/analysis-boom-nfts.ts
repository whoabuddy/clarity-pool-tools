import { network } from "../src/deploy";
import { downloadAccountTxs, logAccountTxsCSV } from "../src/pool-tool-utils";

(async () => {
  await downloadAccountTxs(
    "SP497E7RX3233ATBS2AB9G4WTHB63X5PBSP5VGAQ.boom-nfts",
    network
  );
  await logAccountTxsCSV(
    "SP497E7RX3233ATBS2AB9G4WTHB63X5PBSP5VGAQ.boom-nfts",
    network
  );
})();
