import {
  AnchorMode,
  bufferCVFromString,
  createAssetInfo,
  FungibleConditionCode,
  makeContractCall,
  makeStandardFungiblePostCondition,
  makeStandardSTXPostCondition,
  PostConditionMode,
  someCV,
  standardPrincipalCV,
  uintCV,
} from "@stacks/transactions";
import BN from "bn.js";
import { handleTransaction, network } from "./deploy";

const dryRun = true;

export async function mintInUI(
  sender: string,
  amount: number,
  doContractCall: any,
  userSession: any
) {
  doContractCall({
    contractAddress: "SPN4Y5QPGQA8882ZXW90ADC2DHYXMSTN8VAR8C3X",
    contractName: "friedger-token-v1",
    functionName: "mint",
    functionArgs: [uintCV(amount)],
    userSession,
    network,
    anchorMode: AnchorMode.Any,
    postConditionMode: PostConditionMode.Deny,
    postConditions: [
      makeStandardSTXPostCondition(
        sender,
        FungibleConditionCode.Equal,
        new BN(amount)
      ),
    ],
  });
  return;
}

export async function mint(
  amount: number,
  user: { stacks: string; private: string }
) {
  const tx = await makeContractCall({
    contractAddress: "SPN4Y5QPGQA8882ZXW90ADC2DHYXMSTN8VAR8C3X",
    contractName: "friedger-token-v1",
    functionName: "mint",
    functionArgs: [uintCV(amount)],
    network,
    anchorMode: AnchorMode.Any,
    postConditionMode: PostConditionMode.Deny,
    postConditions: [
      makeStandardSTXPostCondition(
        user.stacks,
        FungibleConditionCode.Equal,
        new BN(amount)
      ),
    ],
    fee: new BN(1000),
    nonce: new BN(601),
    senderKey: user.private,
  });
  return await handleTransaction(tx);
}

export async function transferFRIE(
  amount: number,
  to: string,
  user: { stacks: string; private: string },
  nonce: number,
  memo: string
) {
  const tx = await makeContractCall({
    contractAddress: "SPN4Y5QPGQA8882ZXW90ADC2DHYXMSTN8VAR8C3X",
    contractName: "friedger-token-v1",
    functionName: "transfer",
    functionArgs: [
      uintCV(amount),
      standardPrincipalCV(user.stacks),
      standardPrincipalCV(to),
      someCV(bufferCVFromString(memo)),
    ],
    network,
    anchorMode: AnchorMode.Any,
    postConditionMode: PostConditionMode.Deny,
    postConditions: [
      makeStandardFungiblePostCondition(
        user.stacks,
        FungibleConditionCode.Equal,
        new BN(amount),
        createAssetInfo(
          "SPN4Y5QPGQA8882ZXW90ADC2DHYXMSTN8VAR8C3X",
          "friedger-token-v1",
          "friedger"
        )
      ),
    ],
    fee: new BN(800),
    nonce: new BN(nonce),
    senderKey: user.private,
  });
  if (dryRun) {
    return "dryRun " + tx.txid();
  } else {
    return await handleTransaction(tx);
  }
}
